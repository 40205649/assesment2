﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for A_Guest.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class A_Guest : Window
    {
        private MainWindow mywindow3;
        //passing a reference to the mainwindow
        public A_Guest(MainWindow win3)
        {
            InitializeComponent();
            mywindow3 = win3;
            //any changes made will be changed in main window
        }

        private void Guest_OK_Button_Click(object sender, RoutedEventArgs e)
        {
            //Declare Lists
            Storage_Guest newGuest = new Storage_Guest();
            //for all bookings
            for (int i = 0; i < mywindow3.bookings.Count; i++)
            {
                // for each Guests
                foreach (Storage_Guest g in mywindow3.newGuest)
                {
                    //If the guest Name Exists
                    if (g.Gname == A_Guest_Name_Textbox.Text)
                    {
                        //ID textbox displays Booking ID
                        A_Guest_ID_Textbox.Text = Convert.ToString(mywindow3.bookings[i].BID);
                        //guest name textbox displays guest name
                        A_Guest_Name_Textbox.Text = g.Gname;
                        //Guest passport textbox displays passport Number
                        A_Guest_Passport_Textbox.Text = g.Passport;
                        //Age Textbox Displays age
                        A_Guest_Age_Textbox.Text = g.Age;
                    }
                }
            }
        }
        private void A_Extras_Close_Click(object sender, RoutedEventArgs e)
        {
            //for all bookings
            for (int i = 0; i < mywindow3.bookings.Count; i++)
            {
                // for each guest
                foreach (Storage_Guest g in mywindow3.newGuest)
                {
                    // if the guest name exists
                    if (g.Gname == A_Guest_Name_Textbox.Text)
                    {
                        //guest name is equal to the textbox info
                        g.Gname = A_Guest_Name_Textbox.Text;
                        //Guest passport is equal to the textbox info
                        g.Passport = A_Guest_Passport_Textbox.Text;
                        //guest age is equal to the textbox info
                        g.Age = A_Guest_Age_Textbox.Text;
                    }
                    //Close the window
                    this.Close();
                }
            }
        }
    }
}
