﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for D_Booking.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class D_Booking : Window
    {
        private MainWindow mywindow5;
        //passing a reference to the mainwindow
        public D_Booking(MainWindow win5)
        {
            InitializeComponent();
            mywindow5 = win5;
            //changes will be made in main window
        }

        private void Booking_OK_Button_Click(object sender, RoutedEventArgs e)
        {
            //Declare booking List
            Storage_Booking booking = new Storage_Booking();
            //For all bookings
            for (int i = 0; i < mywindow5.bookings.Count; i++)
            {
                //if the ID exists
                if (Convert.ToString(mywindow5.bookings[i].BID) == D_Booking_ID_Textbox.Text)
                {
                    // textbox displays the booking ID
                    D_Booking_ID_Textbox.Text = Convert.ToString(mywindow5.bookings[i].BID);
                    //The datepicker will display the date chosen
                    D_Booking_Datepicker.SelectedDate = mywindow5.bookings[i].Date1;
                    //The datepciker 2 will display the second date chosen
                    D_Booking_Datepicker2.SelectedDate = mywindow5.bookings[i].Date2;
                }
            }
        }
    }
}
