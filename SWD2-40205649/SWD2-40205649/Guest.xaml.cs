﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for Guest.xaml
    /// </summary>
    /// //Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class Guest : Window
    {

        private MainWindow mywindow3;
        //passing a reference to the mainwindow
        public Guest(MainWindow win3)
        {
            InitializeComponent();
            mywindow3 = win3;
            //any changes made will be changed in main window
        }

        private void Guest_OK_Button_Click(object sender, RoutedEventArgs e)
        {
            //Declare list
            Storage_Guest newGuest = new Storage_Guest();
            //ID Is equal to the ID textbox
            newGuest.BID = Convert.ToInt32(Guest_ID_Textbox.Text);
            //if null
            if (newGuest.Gname == null)
            {
                MessageBox.Show("Please enter date");
            }
            else
            {
                //Guest name is equal to the Guest Name Textbox
                newGuest.Gname = Guest_Name_Textbox.Text;
            }
            if (newGuest.Passport == null)
            {
                MessageBox.Show("Please enter date");
            }
            else
            {
                //Passport is equal to the passport textbox
                newGuest.Passport = Guest_Passport_Textbox.Text;
            }
            if (newGuest.Age == null)
            {
                MessageBox.Show("Please enter date");
            }
            else
            {
                if (Convert.ToInt32(newGuest.Age) > 101 || Convert.ToInt32(newGuest.Age) < 0)
                {
                    MessageBox.Show("Age out of Range please re-enter");
                }
                else
                {
                    // age is equal to the age textbox
                    newGuest.Age = Guest_Age_Textbox.Text;
                }
                //pass values bnack to main window
                mywindow3.AddGuest(newGuest);
                // for all bookings
                foreach (Storage_Booking b in mywindow3.bookings)
                {
                    //ifd the booking ID is equal to the input ID
                    if (b.BID == newGuest.BID)
                    {
                        //Link Guests to booking
                        b.guests.Add(newGuest);
                        b.BID = newGuest.BID;
                    }
                }
                //Close window
                this.Close();
            }
        }
    }
}
