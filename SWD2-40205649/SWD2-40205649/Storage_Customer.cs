﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//listener
//references to the onjects in the arraylist
////Hector-Macleod
/// 40205649
/// last edited 23:00 9/12/2016
namespace SWD2_40205649
{
    public class Storage_Customer
    {
        //Customer has access to a list of Bookings
        public List<Storage_Booking> bookings = new List<Storage_Booking>();

        //declare variables
        private string cname;
        private string address;
        private int cID;
        //declare gets and sets
        public string Cname
        {
            get { return cname; }
            set {

                // here's where you write the check
                // throw exceptions

               // if (value == null)
               // {
               //     throw new ArgumentNullException();
              //  }
                cname = value; }
        }

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        public int CID
        {
            get { return cID; }
            set { cID = value; }
        }
    }
}
