﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for D_Guest.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class D_Guest : Window
    {
        private MainWindow mywindow5;
        //passing a reference to the mainwindow
        public D_Guest(MainWindow win5)
        {
            InitializeComponent();
            mywindow5 = win5;
           //any changes made will be changed in main window

        }

        private void D_Ok_Button_Click(object sender, RoutedEventArgs e)
        {
            //Declare List
            Storage_Guest newGuest = new Storage_Guest();
            // for all bookings
            for (int i = 0; i < mywindow5.bookings.Count; i++)
            {
                //for all guests
                foreach (Storage_Guest g in mywindow5.newGuest)
                {
                    //if the name is equal tot the name in the textbox
                    if (g.Gname == D_Name_Textbox.Text)
                    {
                        //the booking textbox is equal to the Booking ID
                        D_Booking_Textbox.Text = Convert.ToString(mywindow5.bookings[i].BID);
                        // Name textbox is equal to the  guest name
                        D_Name_Textbox.Text = g.Gname;
                        //Passport textbox is equal to the Guest Passport
                        D_Passport_Textbox.Text = g.Passport;
                        //Age textbox is equal to the Guest age
                        D_Age_Textbox.Text = g.Age;

                    }
                }
            }
        }

        private void D_Close_Button_Click(object sender, RoutedEventArgs e)
        {
            //declare index int
            int index = 1;
            //Declare Booking index
            int bookingIndex = 1;
            //Declare Guest List
            Storage_Guest guest = new Storage_Guest();
            //For all bookings
            for (int i = 0; i < mywindow5.bookings.Count; i++)
            {
                // for all guests in Bookings
                foreach (Storage_Guest g in mywindow5.bookings[i].guests)
                {
                    //If the guest name is equal to the textbox  and the bookings is equal to the booking textbox
                    if (g.Gname == D_Name_Textbox.Text && mywindow5.bookings[i].BID == int.Parse(D_Booking_Textbox.Text))
                    {                      
                        //booking index is equal to i       
                        bookingIndex = i;
                        // index is equal to the the booking
                        index = mywindow5.newGuest.IndexOf(g);
                        //guest is equal to g
                        guest = g;
                    }
                }
            }
            //remove guest
            mywindow5.bookings[bookingIndex].guests.Remove(guest);
            //close window
            this.Close();
        }
    }
}