﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for Amend.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class Amend : Window
    {
        MainWindow mainwindow = null;
        //passing a reference to the mainwindow
        public Amend(MainWindow main_window)
        {
            
            InitializeComponent();
            //any changes made will be changed in main window
            this.mainwindow = main_window;
        }

        private void Customer_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called A_customer
            //pass a reference to mainwindow
            var A_Customer = new A_Customer(mainwindow);
            //A_customer window can only be accessed when opened until closed
            A_Customer.Show();
        }

        private void Booking_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called A_Booking
            //pass a reference to mainwindow
            var A_Booking = new A_Booking(mainwindow);
            //A_Booking window can only be accessed when opened until closed
            A_Booking.Show();
        }

        private void Guest_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called A_Guest
            //pass a reference to mainwindow
            var A_Guest = new A_Guest(mainwindow);
            //A_Guest window can only be accessed when opened until closed
            A_Guest.Show();
        }

        private void Extras_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called A_Extra
            //pass a reference to mainwindow
            var A_Extras = new A_Extras(mainwindow);
            //A_Extra window can only be accessed when opened until closed
            A_Extras.Show();

        }
    }
}
