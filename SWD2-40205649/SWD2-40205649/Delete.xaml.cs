﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for Delete.xaml
    /// </summary>
    // Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class Delete : Window
    {
        MainWindow mainwindow = null;
        //passing a reference to the mainwindow
        public Delete(MainWindow Main_Window)
        {
            InitializeComponent();
            this.mainwindow = Main_Window;
            //any changes made will be changed in main window
        }

        private void Customer_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called DeleteC
            //pass a reference to mainwindow
            var DeleteC = new D_Customer(mainwindow);
            //DeleteC window can only be accessed when opened until closed
            DeleteC.ShowDialog();
            //no window can be accessed once this is opened 
        }

        private void Booking_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called DeleteB
            //pass a reference to mainwindow
            var DeleteB = new D_Booking(mainwindow);
            //DeleteB window can only be accessed when opened until closed
            DeleteB.ShowDialog();
            //no window can be accessed once this is opened
        }

        private void Guest_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called DeleteG
            //pass a reference to mainwindow
            var DeleteG = new D_Guest(mainwindow);
            //DeleteG window can only be accessed when opened until closed
            DeleteG.ShowDialog();
            //no window can be accessed once this is opened
        }

        private void Extras_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called DeleteE
            //pass a reference to mainwindow
            var DeleteE = new D_Extra();
            //DeleteE window can only be accessed when opened until closed
            DeleteE.ShowDialog();
            //no window can be accessed once this is opened
        }
    }
}
