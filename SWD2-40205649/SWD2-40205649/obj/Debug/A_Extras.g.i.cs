﻿#pragma checksum "..\..\A_Extras.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "293CD32D48CB8F879ABE0FCBD1106587"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace SWD2_40205649 {
    
    
    /// <summary>
    /// A_Extras
    /// </summary>
    public partial class A_Extras : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 6 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label;
        
        #line default
        #line hidden
        
        
        #line 7 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox A_Extra_ID_Textbox;
        
        #line default
        #line hidden
        
        
        #line 8 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox A_Extra_Meals_ComboBox;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox A_Extras_Breakfast_Textbox;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox A_Extra_Breakfast_ComboBox;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1_Copy;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox A_Extras_Meals_Textbox1;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2_Copy1;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker A_Extra_Datepicker_Car;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label3;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker A_Extra_Datepicker_Car2;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label4;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label5;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button A_Extras_OK_Button;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\A_Extras.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button A_close;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/SWD2-40205649;component/a_extras.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\A_Extras.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.label = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.A_Extra_ID_Textbox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.A_Extra_Meals_ComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 4:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 5:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.A_Extras_Breakfast_Textbox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.A_Extra_Breakfast_ComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.label1_Copy = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.A_Extras_Meals_Textbox1 = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.label2_Copy1 = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.A_Extra_Datepicker_Car = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 12:
            this.label3 = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.A_Extra_Datepicker_Car2 = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 14:
            this.label4 = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.label5 = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.A_Extras_OK_Button = ((System.Windows.Controls.Button)(target));
            
            #line 27 "..\..\A_Extras.xaml"
            this.A_Extras_OK_Button.Click += new System.Windows.RoutedEventHandler(this.Extras_OK_Button_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.A_close = ((System.Windows.Controls.Button)(target));
            
            #line 28 "..\..\A_Extras.xaml"
            this.A_close.Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

