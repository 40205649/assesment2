﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//use tostring
namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for Booking.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class Booking : Window
    {
        private MainWindow mywindow2;
        //passing a reference to the mainwindow
        public Booking(MainWindow win2)
        {
            //any changes made will be changed in main window
            
            InitializeComponent();
            mywindow2 = win2;
        }

        private void Booking_OK_Button_Click(object sender, RoutedEventArgs e)
        {
            //declare booking list
            Storage_Booking newbooking = new Storage_Booking();
            //ID is equal to the ID textbox
            newbooking.BID = Convert.ToInt32(Booking_ID_Textbox.Text);
            //Arrival Date is equal to the first datepicker
            //if null
            if (newbooking.Date1 == null)
            {
                MessageBox.Show("Please enter date");
            }
            else
            {
                newbooking.Date1 = (DateTime)Booking_Datepicker.SelectedDate;
                //Arrival Date is equal to the second datepicker
            }
            //if null
            if (newbooking.Date2 == null)
            {
                MessageBox.Show("Please enter date");
            }
            else
            {
                newbooking.Date2 = (DateTime)Booking_Datepicker2.SelectedDate;
            }
            //pass the values through to the AddBooking method
            mywindow2.AddBooking(newbooking);
            //Close the window
            this.Close();
        }
    }
}
