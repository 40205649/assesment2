﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for A_Booking.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class A_Booking : Window
    {
        private MainWindow mywindow2;
        //passing a reference to the mainwindow
        public A_Booking(MainWindow win2)
        {
            
            InitializeComponent();
            mywindow2 = win2;
            //any changes made will be changed in main window
        }

        private void Booking_OK_Button_Click(object sender, RoutedEventArgs e)
        {
            //Declare booking List
            Storage_Booking booking = new Storage_Booking();
            //For all bookings
            for (int i = 0; i < mywindow2.bookings.Count; i++)
            {
                //if the ID exists
                if (Convert.ToString(mywindow2.bookings[i].BID) == A_Booking_ID_Textbox.Text)
                {
                    // textbox displays the booking ID
                    A_Booking_ID_Textbox.Text = Convert.ToString(mywindow2.bookings[i].BID);
                    //The datepicker will display the date chosen
                    A_Booking_Datepicker.SelectedDate = mywindow2.bookings[i].Date1;
                    //The datepciker 2 will display the second date chosen
                    A_Booking_Datepicker2.SelectedDate = mywindow2.bookings[i].Date2;
                }
            }
        }

        private void A_Booking_Close_Click(object sender, RoutedEventArgs e)
        {
            //Declare List
            Storage_Booking newbooking = new Storage_Booking();
            //For all bookings
            for (int i = 0; i < mywindow2.bookings.Count; i++)
            {
                //If the ID exists
                if (Convert.ToString(mywindow2.bookings[i].BID) == A_Booking_ID_Textbox.Text)
                {
                    //The datepicker will save any changes made
                    mywindow2.bookings[i].Date1 = (DateTime)A_Booking_Datepicker.SelectedDate;
                    //The datepicker 2 will save any changes made
                    mywindow2.bookings[i].Date2 = (DateTime)A_Booking_Datepicker2.SelectedDate;
                }
                //closes window
                this.Close();
            }
        }
    }
}
 
 
