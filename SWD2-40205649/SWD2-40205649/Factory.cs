﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWD2_40205649
{
    //Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public class IDFactory
    {
        // create variables to increment ID'S
            int bookingID = 1;
            int customerID = 1;
            // gets booking id
            //get customer id
            //Increment  Booking ID Method
            public int GetBookingID()
            {
                //id when called is increment every time
                //eg 5 bookings will mean the next ID will be 6
                int newID = bookingID;
                bookingID++;
                return newID;
            }
        //Increment Customer ID Method 
        public int GetCustomerID()
            {
            //Increment Customer ID Method increment every time
            //eg 6 customers will mean the next ID will be 7
            int newID = customerID;
                customerID++;
                return newID; 
            }
    }
}
