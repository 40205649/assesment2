﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWD2_40205649
{
    //Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
     public class Storage_Extra
    {
        //Declare all variables
        private int bID;
        private string eMeals;
        private string bMeals;
        private string dietary1;
        private string dietary2;
        private DateTime datepicker1;
        private DateTime datepicker2;
        //Declare Gets and Sets
        public string EMeals
        {
            get { return eMeals; }
            set { eMeals = value; }
        }

        public string BMeals
        {
            get { return bMeals; }
            set { bMeals = value; }
        }

        public string Dietary1
        {
            get { return dietary1; }
            set { dietary1 = value; }
        }

        public string Dietary2
        {
            get { return dietary2; }
            set { dietary2 = value; }
        }
        public DateTime Datepicker1
        {
            get { return datepicker1; }
            set { datepicker1 = value; }
        }

        public DateTime Datepicker2
        {
            get { return datepicker2; }
            set { datepicker2 = value; }
        }

        public int BID
        {
            get { return bID; }
            set { bID = value; }
        }


    }
}
