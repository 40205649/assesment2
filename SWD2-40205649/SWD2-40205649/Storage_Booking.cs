﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWD2_40205649
{
    //Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public class Storage_Booking
    {
        //booking has access to guests and extra
        public List<Storage_Guest> guests = new List<Storage_Guest>();
        public List<Storage_Extra> extra = new List<Storage_Extra>();

        //declare booking vartiables
        private DateTime date1;
        private DateTime date2;
        private int bID;
        //Declare gets and  sets
        public DateTime Date1
        {
            get { return date1; }
            set { date1 = value; }
        }

        public DateTime Date2
        {
            get { return date2; }
            set { date2 = value; }
        }

        public int BID
        {
            get { return bID; }
            set { bID = value; }
        }
    }
}
