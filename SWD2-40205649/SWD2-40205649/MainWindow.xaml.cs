﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class MainWindow : Window
    {

        // populates lists
        public static IDFactory idfactory = new IDFactory();
        //create lists to store data for bookings, customers, guests and extras
        public List<Storage_Booking> bookings = new List<Storage_Booking>();
        public List<Storage_Customer> customers = new List<Storage_Customer>();
        public List<Storage_Guest> newGuest = new List<Storage_Guest>();
        public List<Storage_Extra> extra = new List<Storage_Extra>();

        //add ccs class of struct of booking contain ing aall relative information
        public void main()
        {
            //factory methods for booking and customer ID
            int bookingID = idfactory.GetBookingID();
            int customerID = idfactory.GetCustomerID();
        }


        public MainWindow()
        {
            InitializeComponent();

        }

        private void Amend_Button_Click(object sender, RoutedEventArgs e)
        {
            //open new window called amend
            var Amend = new Amend(this);
            //only window that can be accessed once opened

            Amend.ShowDialog();

        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            //open new window called Delete
            var Delete = new Delete(this);
            //open window that can only be accessed onced opened until closed
            Delete.ShowDialog();
        }

        private void Invoice_Button_Click(object sender, RoutedEventArgs e)
        {
            //only new window called Invoice
            var Invoice = new Invoice();
            //open window that can only be accessed onced opened until closed
            Invoice.ShowDialog();
        }

        private void Add_Button_Click(object sender, RoutedEventArgs e)
        {
            //open window called Add
            var Add = new Add(this);
            //open window that can only be accessed onced opened until closed
            Add.ShowDialog();
        }

        public void AddCustomer(Storage_Customer newCust)
        {
            //factory created id for customer is created
            newCust.CID = idfactory.GetCustomerID();
            //add customer details to list
            customers.Add(newCust);
        }

        public void AddBooking(Storage_Booking newbooking)
        {
            //factory created id for booking is created
            newbooking.BID = idfactory.GetBookingID();
            //add booking details to booking list
            bookings.Add(newbooking);
        }

        public void AddGuest(Storage_Guest newguest)
        {
            //Add guest details to guest list
            newGuest.Add(newguest);
        }

        public void AddExtra(Storage_Extra newextra)
        {
            //Add extra details to extra list
            extra.Add(newextra);
        }

        public void RemoveGuest(Storage_Guest newguest)
        {
            //remove guest details from guest list - partially working can retrieve
            //and should be deleting 
            newGuest.Remove(newguest);
                
        }
    }

}

