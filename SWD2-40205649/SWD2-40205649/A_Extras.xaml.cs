﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for A_Extras.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class A_Extras : Window
    {
        private MainWindow mywindow4;
        //passing a reference to the mainwindow
        public A_Extras(MainWindow win4)
        {
            InitializeComponent();
            mywindow4 = win4;
            //any changes made will be changed in main window
        }


        private void Extras_OK_Button_Click(object sender, RoutedEventArgs e)
        {
            //Declare List
            Storage_Extra newExtra = new Storage_Extra();
            //For all Extras
            for (int i = 0; i < mywindow4.extra.Count; i++)
            {
                //For each customer contained
                foreach (Storage_Extra d in mywindow4.extra)
                {
                    {
                        //if the Id Entered Exists
                        if (Convert.ToString(mywindow4.extra[i].BID) == A_Extra_ID_Textbox.Text)
                        {
                            //Customer ID is equal to the ID in the textbox
                            A_Extra_ID_Textbox.Text = Convert.ToString(mywindow4.extra[i].BID);
                            //Evening meals is equal to evening meals ComboBox
                            A_Extra_Meals_ComboBox.Text = d.EMeals;
                            //Dietary Requirment is equal to the dietary textbox
                            A_Extras_Meals_Textbox1.Text = d.Dietary1;
                            //Breakfast meals is equal to Breakfast meals ComboBox
                            A_Extra_Breakfast_ComboBox.Text = d.BMeals;
                            //Dietary Requirment2 is equal to the dietary textbox2
                            A_Extras_Breakfast_Textbox.Text = d.Dietary2;
                            //datepicker one equal to the start datepicker
                            A_Extra_Datepicker_Car.SelectedDate = d.Datepicker1;
                            //datepicker 2 is equal to the end datepicker
                            A_Extra_Datepicker_Car2.SelectedDate = d.Datepicker2;
                        }
                    }
                }
            }


        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Declare List
            Storage_Extra newExtra = new Storage_Extra();
            //For all Extras
            for (int i = 0; i < mywindow4.extra.Count; i++)
            {
                //For each customer contained
                foreach (Storage_Extra d in mywindow4.extra)
                {
                    {
                        //if the Id Entered Exists
                        if (Convert.ToString(mywindow4.extra[i].BID) == A_Extra_ID_Textbox.Text)
                        {
                            //Customer ID is equal to the ID in the textbox
                            A_Extra_ID_Textbox.Text = Convert.ToString(mywindow4.extra[i].BID);
                            //Evening Meals is Saved based on ComboBox
                            d.EMeals = A_Extra_Meals_ComboBox.Text;
                            //Diet1 is saved based on diet textbox
                            d.Dietary1 = A_Extras_Meals_Textbox1.Text;
                            //Breakfast Meals is saved based on whatever is in ComboBox
                            d.BMeals = A_Extra_Breakfast_ComboBox.Text;
                            //Diet2 is saved based on Diet2 Textbox
                            d.Dietary2 = A_Extras_Breakfast_Textbox.Text;
                            //Datepicker 1 is saved based Upon  Datepicker
                            d.Datepicker1 = (DateTime)A_Extra_Datepicker_Car.SelectedDate;
                            //Datepicker2 is saved based Upon  Datepicker2
                            d.Datepicker2 = (DateTime)A_Extra_Datepicker_Car2.SelectedDate;
                        }
                    }
                }
            }
            //Closes Window
            this.Close();
        }
    }
}
