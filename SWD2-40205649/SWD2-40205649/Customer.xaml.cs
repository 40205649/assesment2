﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for Customer.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class Customer : Window
    {
        //passing a reference to the mainwindow
        private MainWindow mywindow;
       
        public Customer(MainWindow win)
        {
            //any changes made will be changed in main window
            InitializeComponent();
            mywindow = win;
        }
      
        private void Customer_OK_Button_Click(object sender, RoutedEventArgs e)
        {
            //Declare Customer List
            Storage_Customer newCustomer = new Storage_Customer();
            newCustomer.CID = Convert.ToInt32(Customer_ID_Textbox.Text);
            //if name is null
            if (newCustomer.Cname == null)
            {
                MessageBox.Show("Cannot Be left empty");
            }
            else
            {
                // Customer Name is equal to Name textbox
                newCustomer.Cname = Name_Id_Textbox.Text;
            }
            //if name is null
            if (newCustomer.Address == null)
            {
                MessageBox.Show("Cannot Be left empty");
            }
            else
            {
                //address is equal to the address textbox
                newCustomer.Address = Customer_Address_Textbox.Text;
            }
            //pass values to main window         
            mywindow.AddCustomer(newCustomer);
            //close window
            this.Close();
        }
    }
}
