﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for Add.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class Add : Window
    {
       

        MainWindow mainwindow = null;
        //passing a reference to the mainwindow
        public Add(MainWindow main_window)
        {
            //any changes made will be changed in main window
            InitializeComponent();
            this.mainwindow = main_window;
        }

        private void Customer_Button_Click(object sender, RoutedEventArgs e)
        {    
            //create a window called customer
            //pass a reference to mainwindow
            var Customer = new Customer(mainwindow);
            //customer window can only be accessed when opened until closed
            Customer.ShowDialog();
        }

        private void Booking_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called Booking
            //pass a reference to mainwindow
            var Booking = new Booking(mainwindow);
            //Booking window can only be accessed when opened until closed
            Booking.ShowDialog();
        }

        private void Guest_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called Guest
            //pass a reference to mainwindow
            var Guest = new Guest(mainwindow);
            //Guest window can only be accessed when opened until closed
            Guest.ShowDialog();
        }

        private void Extras_Button_Click(object sender, RoutedEventArgs e)
        {
            //create a window called Extra
            //pass a reference to mainwindow
            var Extra = new Extra(mainwindow);
            //Extra window can only be accessed when opened until closed
            Extra.ShowDialog();
        }
    }
}
