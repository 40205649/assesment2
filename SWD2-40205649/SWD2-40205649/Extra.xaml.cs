﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for Extra.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class Extra : Window
    {
        private MainWindow mywindow4;
        //passing a reference to the mainwindow
        public Extra(MainWindow win4)
        {
            InitializeComponent();
            mywindow4 = win4;
            //any changes made will be changed in main window
        }



        private void Extras_OK_Button_Click(object sender, RoutedEventArgs e)
        {
            //Declare extra list
            Storage_Extra newExtra = new Storage_Extra();
            //ID is equal to textbox
            newExtra.BID = Convert.ToInt32(Extra_ID_Textbox.Text);
            //if null
            if (newExtra.EMeals == null)
            {
                MessageBox.Show("Cannot be left Null");
            }
            else
            {
                //Evening Meal is equal to the evening meal ComboBox
                newExtra.EMeals = Extra_Meals_ComboBox.Text;
            }
            //if null
            if (newExtra.Dietary1 == null)
            {
                MessageBox.Show("Cannot be left Null");
            }
            else
            {
                //Diet description is equal to the extra meals textbox
                newExtra.Dietary1 = Extras_Meals_Textbox1.Text;
            }
             //if null
            if (newExtra.BMeals == null)
            {
                MessageBox.Show("Cannot be left Null");
            }
            else
            {
                //Breakfast Meal is equal to the evening meal ComboBox  
                newExtra.BMeals = Extra_Breakfast_ComboBox.Text;
            }
             //if null
            if (newExtra.Dietary2 == null)
            {
                MessageBox.Show("Cannot be left Null");
            }
            else
            {
                //Diet description2 is equal to the extra meals textbox
                newExtra.Dietary2 = Extras_Breakfast_Textbox.Text;
            }
              //if null
            if (newExtra.Datepicker1 == null)
            {
                MessageBox.Show("Cannot be left Null");
            }
            else
            {
                //car start Date is equal to the first DatePicker
                newExtra.Datepicker1 = (DateTime)Extra_Datepicker_Car.SelectedDate;
            }
              //if null
            if (newExtra.Datepicker2 == null)
            {
                MessageBox.Show("Cannot be left Null");
            }
            else
            {
                //car end Date is equal to the first DatePicker
                newExtra.Datepicker2 = (DateTime)Extra_Datepicker_Car2.SelectedDate;
            }
            //pass extra to main window
            mywindow4.AddExtra(newExtra);
            //close window
            this.Close();          
        }
    }
}
