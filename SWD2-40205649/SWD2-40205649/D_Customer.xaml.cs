﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for D_Customer.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class D_Customer : Window
    {
        //pass ref to main window
        private MainWindow mywindow;
        public D_Customer(MainWindow win)
        {
            InitializeComponent();
            mywindow = win;
            // any changes will be passed to main window
        }

        private void D_Ok_Customer_Button_Click(object sender, RoutedEventArgs e)
        {
            Storage_Customer newCustomer = new Storage_Customer();
            //For all Customers
            for (int i = 0; i < mywindow.customers.Count; i++)
            {
                //For each customer contained
                foreach (Storage_Customer c in mywindow.customers)
                {
                    {
                        //if the Id Entered Exists
                        if (Convert.ToString(mywindow.customers[i].CID) == D_Customer_ID_Textbox.Text)
                        {
                            //Customer ID is equal to the ID in the textbox
                            D_Customer_ID_Textbox.Text = Convert.ToString(mywindow.customers[i].CID);
                            //Customer Name is equal to the name textbox
                            D_CName_Textbox.Text = c.Cname;
                            //Address is equal to the adress textbox
                            D_Address_Textbox.Text = c.Address;
                        }
                    }
                }
            }
        }

        private void D_Close_Customer_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
