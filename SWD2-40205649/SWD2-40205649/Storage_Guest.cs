﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWD2_40205649
{
    //Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public class Storage_Guest
    {
        //Declare Variables
        private int bID;
        private string gname;
        private string passport;
        private string age;

        //declare Gets and Sets
        public int BID
        {
            get { return bID; }
            set { bID = value; }
        }

        public string Gname
        {
            get { return gname; }
            set { gname = value; }
        }

        public string Passport
        {
            get { return passport; }
            set { passport = value; }
        }

        public string Age
        {
            get { return age; }
            set { age = value; }
        }       
    }
}