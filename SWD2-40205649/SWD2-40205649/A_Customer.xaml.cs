﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SWD2_40205649
{
    /// <summary>
    /// Interaction logic for A_Customer.xaml
    /// </summary>
    /// Hector-Macleod
    /// 40205649
    /// last edited 23:00 9/12/2016
    public partial class A_Customer : Window
    {
        //passing a reference to the mainwindow
        private MainWindow mywindow;

        public A_Customer(MainWindow win)
        {
            mywindow = win;
            InitializeComponent();
            //any changes made will be changed in main window
        }

        private void Customer_OK_Button_Click(object sender, RoutedEventArgs e)
        {
            //Declare List
            Storage_Customer newCustomer = new Storage_Customer();
            //For all Customers
            for (int i = 0; i < mywindow.customers.Count; i++)
            {
                //For each customer contained
                foreach (Storage_Customer c in mywindow.customers)
                {
                    {
                        //if the Id Entered Exists
                        if (Convert.ToString(mywindow.customers[i].CID) == A_Customer_ID_Textbox.Text)
                        {
                            //Customer ID is equal to the ID in the textbox
                            A_Customer_ID_Textbox.Text = Convert.ToString(mywindow.customers[i].CID);
                            //Customer Name is equal to the name textbox
                            A_Name_Id_Textbox.Text = c.Cname;
                            //Address is equal to the adress textbox
                            A_Customer_Address_Textbox.Text = c.Address;
                        }
                    }
                }
            }
        }

        private void A_Close_Customer_Click(object sender, RoutedEventArgs e)
        {
            //Declare List
            Storage_Customer newCustomer = new Storage_Customer();
            //For all Customers
            for (int i = 0; i < mywindow.customers.Count; i++)
            {
                //For Each Customer
                foreach (Storage_Customer c in mywindow.customers)
                {
                    //if the Id Entered Exists
                    if (Convert.ToString(mywindow.customers[i].CID) == A_Customer_ID_Textbox.Text)
                    {
                        //The ID is equal to the ID in the textbox
                        c.CID = Convert.ToInt32(A_Customer_ID_Textbox.Text);
                        //The Customer name is saved as whatever is in the textbox
                        c.Cname = A_Name_Id_Textbox.Text;
                        //the Customer address is saved as whatever is in the textbox
                        c.Address = A_Customer_Address_Textbox.Text;
                    }
                    //Close the window
                    this.Close();
                }
            }
        }
    }
}


